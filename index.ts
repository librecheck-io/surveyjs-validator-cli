import Survey from "survey-core";
import { parse } from "https://deno.land/std@0.194.0/flags/mod.ts";
const decoder = new TextDecoder();

function validateJson(schema: object, data: object) {
  const survey = new Survey.Model(schema)
  survey.data = data
  const isValid = survey.validate();
  const errors = survey.pages.flatMap((page) => page.getQuestions(true).filter(question => question.hasErrors()).flatMap(question => question.errors))
  const result = {valid: isValid, errors: errors}
  Deno.stdout.writeSync(new TextEncoder().encode(JSON.stringify(result)))
}

const flags = parse(Deno.args, {
  string: ["input"],
  default: { input: "-" },
});

const toRead = flags.input !== "-" ? (await Deno.open(flags.input)) : Deno.stdin;

for await (const chunk of toRead.readable) {
  const inputText = decoder.decode(chunk);
  try {
    const parsedInput = JSON.parse(inputText)
    if(parsedInput.schema.elements.length > 0 && parsedInput.data){
      validateJson(parsedInput.schema, parsedInput.data)
    }
  }catch(exception){
    console.log(exception)
  }
}